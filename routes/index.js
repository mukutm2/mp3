module.exports = function (app) {
    // Create separate routers for each module
    let usersRouter = require('express').Router();
    let tasksRouter = require('express').Router();
    let homeRouter = require('express').Router();

    require('./users')(usersRouter);
    require('./tasks')(tasksRouter);
    require('./home')(homeRouter);

    app.use('/api/users', usersRouter);
    app.use('/api/tasks', tasksRouter);
    app.use('/api', homeRouter);

    //new push
};
