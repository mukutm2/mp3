// src/routes/tasks.js


var Task = require('../models/task');
var User = require('../models/user')

module.exports = function (router) {
    // List all tasks - GET /api/tasks
    router.get('/', async (req, res) => {
        try {
            let query = Task.find({});

            // Apply query string parameters
            if (req.query.where) query.where(JSON.parse(req.query.where));
            if (req.query.sort) query.sort(JSON.parse(req.query.sort));
            if (req.query.select) query.select(JSON.parse(req.query.select));
            if (req.query.skip) query.skip(parseInt(req.query.skip));
            if (req.query.limit) query.limit(parseInt(req.query.limit) || 100); // Default limit for tasks is 100

            // Count documents if count parameter is true
            if (req.query.count && req.query.count === 'true') {
                const count = await Task.countDocuments(query.getFilter()).exec();
                return res.status(200).json({ message: "OK", data: count });
            }

            const tasks = await query.exec();
            res.status(200).json({ message: "OK", data: tasks });
        } catch (err) {
            res.status(500).json({ message: "Server error", data: err });
        }
    });

    // Create a new task - POST /api/tasks
    router.post('/', async (req, res) => {
        try {
            const { name, deadline, assignedUser } = req.body;
            if (!name || !deadline) {
                return res.status(400).json({ message: "Name and deadline are required", data: {} });
            }

            console.log(`Creating task with name: ${name}, deadline: ${deadline}, assignedUser: ${assignedUser}`); // Debugging log

            const newTask = new Task(req.body);
            await newTask.save();

            if (assignedUser) {
                await User.findByIdAndUpdate(assignedUser, { $addToSet: { pendingTasks: newTask._id } });
            }

            res.status(201).json({ message: "Task created", data: newTask });
        } catch (err) {
            console.error("Error creating task:", err); // Log the error
            res.status(500).json({ message: "Server error", data: err });
        }
    });


    // Get a specific task - GET /api/tasks/:id
    router.get('/:id', async (req, res) => {
        try {
            let query = Task.findById(req.params.id);
            if (req.query.select) query.select(JSON.parse(req.query.select));

            const task = await query.exec();
            if (!task) {
                return res.status(404).json({ message: "Task not found", data: {} });
            }

            res.status(200).json({ message: "OK", data: task });
        } catch (err) {
            res.status(500).json({ message: "Server error", data: err });
        }
    });

    // Update a specific task - PUT /api/tasks/:id
    router.put('/:id', async (req, res) => {
        try {
            const task = await Task.findById(req.params.id);
            if (!task) {
                return res.status(404).json({ message: "Task not found", data: {} });
            }

            const oldAssignedUser = task.assignedUser;
            const newAssignedUser = req.body.assignedUser;

            console.log(`Updating task: ${task._id}`);
            console.log(`Old Assigned User: ${oldAssignedUser}, New Assigned User: ${newAssignedUser}`); // Debugging log

            const updatedTask = await Task.findByIdAndUpdate(req.params.id, req.body, { new: true });

            // Update old user's pendingTasks if assignedUser changed
            if (oldAssignedUser && oldAssignedUser !== newAssignedUser) {
                await User.findByIdAndUpdate(oldAssignedUser, { $pull: { pendingTasks: task._id } });
            }

            // Update new user's pendingTasks
            if (newAssignedUser) {
                await User.findByIdAndUpdate(newAssignedUser, { $addToSet: { pendingTasks: task._id } });
            }

            res.status(200).json({ message: "Task updated", data: updatedTask });
        } catch (err) {
            console.error("Error updating task:", err); // Log the error
            res.status(500).json({ message: "Server error", data: err });
        }
    });

    // Delete a specific task - DELETE /api/tasks/:id
    router.delete('/:id', async (req, res) => {
        try {
            const task = await Task.findById(req.params.id);
            if (!task) {
                return res.status(404).json({ message: "Task not found", data: {} });
            }

            if (task.assignedUser) {
                await User.findByIdAndUpdate(task.assignedUser, { $pull: { pendingTasks: task._id } });
            }

            await task.remove();
            res.status(200).json({ message: "Task deleted", data: {} });
        } catch (err) {
            res.status(500).json({ message: "Server error", data: err });
        }
    });

    return router;
};