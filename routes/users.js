// src/routes/users.js


var User = require('../models/user');
var Task = require('../models/task');

module.exports = function (router) {
    // GET /api/users - List all users
    router.get('/', async (req, res) => {
        try {
            let query = User.find({});

            // Apply query string parameters
            if (req.query.where) query.where(JSON.parse(req.query.where));
            if (req.query.sort) query.sort(JSON.parse(req.query.sort));
            if (req.query.select) query.select(JSON.parse(req.query.select));
            if (req.query.skip) query.skip(parseInt(req.query.skip));
            if (req.query.limit) query.limit(parseInt(req.query.limit));

            // Count documents if count parameter is true
            if (req.query.count && req.query.count === 'true') {
                const count = await User.countDocuments(query.getFilter()).exec();
                return res.status(200).json({ message: "OK", data: count });
            }

            const users = await query.exec();
            res.status(200).json({ message: "OK", data: users });
        } catch (err) {
            res.status(500).json({ message: "Server error", data: err });
        }
    });

    //POST - updated
    router.post('/', async (req, res) => {
        try {
            const { name, email, pendingTasks = [] } = req.body;
            if (!name || !email) {
                return res.status(400).json({ message: "Name and email are required", data: {} });
            }

            console.log(`Creating user with name: ${name}, email: ${email}`); // Debugging log

            const existingUser = await User.findOne({ email });
            if (existingUser) {
                return res.status(400).json({ message: "User with this email already exists", data: {} });
            }

            const newUser = new User(req.body);
            await newUser.save();

            // If there are pendingTasks, update their assignedUser
            if (pendingTasks.length > 0) {
                await Task.updateMany(
                    { _id: { $in: pendingTasks } },
                    { assignedUser: newUser._id, assignedUserName: newUser.name }
                );
            }

            res.status(201).json({ message: "User created", data: newUser });
        } catch (err) {
            console.error("Error creating user:", err); // Log the error
            res.status(500).json({ message: "Server error", data: err });
        }
    });


    // GET /api/users/:id - Get a specific user
    router.get('/:id', async (req, res) => {
        try {
            let query = User.findById(req.params.id);

            // Apply select query if present
            if (req.query.select) query.select(JSON.parse(req.query.select));

            const user = await query.exec();
            if (!user) {
                return res.status(404).json({ message: "User not found", data: {} });
            }

            res.status(200).json({ message: "OK", data: user });
        } catch (err) {
            res.status(500).json({ message: "Server error", data: err });
        }
    });

    // PUT /api/users/:id - Update a specific user
    router.put('/:id', async (req, res) => {
        try {
            const user = await User.findById(req.params.id);
            if (!user) {
                return res.status(404).json({ message: "User not found", data: {} });
            }

            const updatedUser = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });

            // If pendingTasks are updated, synchronize the tasks
            if (req.body.pendingTasks) {
                // Remove user from tasks no longer pending
                const tasksToRemoveUser = user.pendingTasks.filter(taskId => !req.body.pendingTasks.includes(taskId));
                await Task.updateMany({ _id: { $in: tasksToRemoveUser } }, { assignedUser: '', assignedUserName: 'unassigned' });

                // Add user to new pending tasks
                const tasksToAddUser = req.body.pendingTasks.filter(taskId => !user.pendingTasks.includes(taskId));
                await Task.updateMany({ _id: { $in: tasksToAddUser } }, { assignedUser: user._id, assignedUserName: user.name });
            }

            res.status(200).json({ message: "User updated", data: updatedUser });
        } catch (err) {
            res.status(500).json({ message: "Server error", data: err });
        }
    });

    // DELETE /api/users/:id - Delete a specific user
    router.delete('/:id', async (req, res) => {
        try {
            const user = await User.findById(req.params.id);
            if (!user) {
                return res.status(404).json({ message: "User not found", data: {} });
            }

            console.log(`Deleting user: ${user._id}, unassigning tasks`); // Debugging log

            const tasksUpdated = await Task.updateMany({ assignedUser: user._id }, { assignedUser: '', assignedUserName: 'unassigned' });
            console.log(`Unassigned ${tasksUpdated.modifiedCount} tasks from user: ${user._id}`); // Log number of updated tasks

            await user.remove();
            res.status(200).json({ message: "User deleted", data: {} });
        } catch (err) {
            console.error("Error deleting user:", err); // Log the error
            res.status(500).json({ message: "Server error", data: err });
        }
    });


    return router;
};
